// this is the api key that is typed in .env file normally .env isnt uploaded to gitlab but for this project it is
const apiKey = process.env.REACT_APP_API_KEY;
// this simplifies when needing to call or change something on json file
export const createHeaders = () => {
  return {
    "Content-Type": "application/json",
    "x-api-key": apiKey,
  };
};
