import { useUser } from "../state/UserContext";
import { Navigate } from "react-router-dom";
// when no user is connected return to main or start of site
const withAuth = (Component) => (props) => {
  const { user } = useUser();
  if (user !== null) {
    return <Component {...props} />;
  } else {
    return <Navigate to="/" />;
  }
};

export default withAuth;
