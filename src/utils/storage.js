// read storage and use it to log in user
export const storageSave = (key, value) => {
  if (!key) {
    throw new Error("storageSave: no storage key provided");
  }

  if (!value) {
    throw new Error("storageSave: value provided" + key);
  }

  localStorage.setItem(key, JSON.stringify(value));
};

export const storageRead = (key) => {
  const data = localStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }
  return null;
};

export const storageDelete = (key) => {
  localStorage.removeItem(key);
};
