import { NavLink } from "react-router-dom";
import { useUser } from "../state/UserContext";

const Navbar = () => {
  const { user } = useUser();
  // nav bar with that show diffrent content depend if you are logged inn or not
  return (
    <div className="container">
      <nav>
        <div>
          <img
            className="logo"
            src="icon/icon.svg"
            alt="logo" /* downloaded from https://www.flaticon.com/free-icon/google_2875363?term=translator&related_id=2875363 edited to fit the prosject. */
          />
        </div>
        <div>
          <img className="appname" src="icon/appname.svg" alt="appname" />
        </div>
        {user !== null && (
          <>
            <ul>
              <li>
                <NavLink to="/profile">
                  <img
                    className="profilegif"
                    src="icon/profile.gif"
                    alt="profileimagegif"
                  />
                  <p className="profiletext">{user.username}</p>
                </NavLink>
              </li>
            </ul>
          </>
        )}
      </nav>
    </div>
  );
};

export default Navbar;
