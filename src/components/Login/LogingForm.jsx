import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../state/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

const usernameConfig = {
  required: true,
  minLength: 4,
};

const LoginForm = () => {
  // Hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  // local state
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  // side effects
  useEffect(() => {
    if (user !== null) {
      navigate("translator");
    }
  }, [user, navigate]);
  // event handler
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username.toLowerCase());
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };
  // render functions
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }

    if (errors.username.type === "required") {
      return (
        <span>
          <br /> Username is required
        </span>
      );
    }

    if (errors.username.type === "minLength") {
      return (
        <span>
          <br /> Username is too short (minimum 4 characters)
        </span>
      );
    }
  })();

  return (
    <>
      <div className="loginsection">
        <h2>Type your name</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <fieldset>
            <label htmlFor="username">Username: </label>
            <input
              placeholder="What's your name?"
              type="text"
              {...register("username", usernameConfig)}
            />
            {errorMessage}
          </fieldset>
          <button type="submit" disabled={loading}>
            <b>Continue</b>
          </button>
          {loading && <p>welcome</p>}
          {apiError && <p>{apiError}</p>}
        </form>
      </div>
    </>
  );
};

export default LoginForm;
