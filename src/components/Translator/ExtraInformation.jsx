import React from "react";
// simply give out extra infomasjon on the translator page
function ExtraInformation() {
  return (
    <div className="extraInformationContrainer">
      <h2>Translator</h2>
      <p>
        Translate from English to English Sign langauge (ASL) <br /> American
        Sign Language (ASL) is a natural language. It is the primary sign
        <br />
        language used by the deaf and people with hearing impairment in the USA
        and Canada
      </p>
    </div>
  );
}

export default ExtraInformation;
