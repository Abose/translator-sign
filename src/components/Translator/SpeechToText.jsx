import React from "react";
import SpeechRecognition, {
  useSpeechRecognition,
} from "react-speech-recognition";
// add Speech to text
const Dictaphone = () => {
  const {
    transcript,
    listening,
    resetTranscript,
    browserSupportsSpeechRecognition,
  } = useSpeechRecognition();

  if (!browserSupportsSpeechRecognition) {
    return <span>Browser doesn't support speech recognition.</span>;
  }

  const translationbox = document.getElementById("translationbox");

  if (translationbox > 1) {
    resetTranscript();
  }
  // add the transcript to the translationbox
  const savetekst = (transcript) => {
    translationbox.value = transcript;
  };

  const startListening = () =>
    SpeechRecognition.startListening({ savetekst }[ontoggle]);

  return (
    <div>
      <p>
        {listening ? (
          savetekst(transcript) || (
            <img
              autoFocus
              className="recordingactive"
              type="submit"
              src="icon/audio-editing.gif"
              alt="confirm"
            />
          )
        ) : (
          <img
            autoFocus
            className="recording"
            type="submit"
            src="icon/podcastoff2.gif"
            alt="recording"
          />
        )}
      </p>
      <button
        className="recordingbtn"
        onTouchStart={startListening}
        onMouseDown={startListening}
        onTouchEnd={SpeechRecognition.stopListening}
        onMouseUp={SpeechRecognition.stopListening}
      >
        <img
          autoFocus
          className="recordingstatic"
          type="submit"
          src="icon/podcastoff2.gif"
          alt="recording"
        />
      </button>
    </div>
  );
};
export default Dictaphone;
