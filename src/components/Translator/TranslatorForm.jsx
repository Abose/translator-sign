import { useState } from "react";
import { useForm } from "react-hook-form";
import { nanoid } from "nanoid";

const TranslatorForm = ({ onTranslate }) => {
  const { register, handleSubmit } = useForm();
  // take in whats typed from input
  const onSubmit = (event) => {
    if (event) {
      [...event.translations].forEach((latters) => handleSetChildren(latters));
      onTranslate(event.translations);
      const translationbox = document.getElementById("translationbox");
      translationbox.value = "";
    }
  };

  const [children, setChildren] = useState([]);
  // take in each latter and add it to child
  const handleSetChildren = (latters) => {
    const cleaned = latters.replace(/[^a-zA-Z ]/g, "404");
    setChildren((oldArray) => [...oldArray, cleaned.toLowerCase()]);
  };

  return (
    <section className="containerTranslatorForm">
      <form onSubmit={handleSubmit(onSubmit)}>
        <input
          autoFocus
          /*           maxlength="50" if i want to limit it */
          className="translationinput"
          placeholder="Type the Text you wish to translate"
          id="translationbox"
          type="text"
          {...register("translations")}
        />

        <button className="confirmbtn">
          <img
            className="translationbtn"
            type="submit"
            src="icon/confirm.gif"
            alt="confirm"
          />
        </button>
      </form>
      <section className="TranslationSection">
        <ul className="translatedSign">
          {children.map((latters) => (
            <li key={nanoid()}>
              <button>
                <aside>
                  <img src={"img/" + latters + ".png"} alt={latters} />
                </aside>
                <section>
                  <b>{latters.toUpperCase()}</b>
                </section>
              </button>
            </li>
          ))}
        </ul>
        <div className="undertranslation">
          <h2>Translated Sign</h2>
        </div>
        <a href="/">
          <img
            autoFocus
            className="clearBtn"
            src="icon/clear.gif"
            alt="clear"
          />
        </a>
      </section>
    </section>
  );
};

export default TranslatorForm;
