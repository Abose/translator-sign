const ProfileHeader = ({ username }) => {
  // gives a welcome oppon sign in
  return (
    <header>
      <h5> Hello, Welcome Back {username}</h5>
    </header>
  );
};

export default ProfileHeader;
