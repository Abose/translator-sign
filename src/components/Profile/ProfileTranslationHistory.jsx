import ProfileTranslationHistoryitem from "./ProfileTranslationHistoryitem";
// shows the profile history for the last 10 translations
const ProfileTranslationHistory = ({ translations }) => {
  const translationHisotry = translations.map(
    (item, index) =>
      index < 10 && (
        <ProfileTranslationHistoryitem key={index + " - " + item} item={item} />
      )
  );

  return (
    <section className="translatedHistory">
      <h4>Your Translation History</h4>
      <ul>{translationHisotry}</ul>
    </section>
  );
};

export default ProfileTranslationHistory;
