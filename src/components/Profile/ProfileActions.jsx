import { clearHistory } from "../../api/translations";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../state/UserContext";
import { storageDelete, storageSave } from "../../utils/storage";

const ProfileActions = () => {
  const { user, setUser } = useUser();
  // handles when user press log out and delete localstorage
  const handleLogoutClick = () => {
    if (window.confirm("Do you want to Logout")) {
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
    }
  };
  // handle clear history
  const handleClearHistory = async () => {
    if (!window.confirm("Are you sure?\nThis is not reversible")) {
      return;
    }
    const [clearError] = await clearHistory(user.id);

    if (clearError !== null) {
      return;
    }

    const updatedUser = {
      ...user,
      translations: [],
    };

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };

  return (
    <ul>
      <li>
        <button className="clearHistoryBtn" onClick={handleClearHistory}>
          Clear History
        </button>
      </li>
      <li>
        <button className="logoutBtn" onClick={handleLogoutClick}>
          sign out
        </button>
      </li>
    </ul>
  );
};

export default ProfileActions;
