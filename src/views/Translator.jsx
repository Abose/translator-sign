import { addTranslation } from "../api/translations";
import Footer from "../components/Footer/Footer";
import ExtraInformation from "../components/Translator/ExtraInformation";
import Dictaphone from "../components/Translator/SpeechToText";
import TranslatorForm from "../components/Translator/TranslatorForm";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import withAuth from "../hoc/withAuth";
import { useUser } from "../state/UserContext";
import { storageSave } from "../utils/storage";

// translation page

const Translator = () => {
  const { user, setUser } = useUser();

  const handleTranslatedText = async (translations) => {
    if (!translations) {
      alert("Please type something you wish to translate");
      return;
    }
    const [error, updatedUser] = await addTranslation(user, translations);
    if (error !== null) {
      return;
    }

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };

  return (
    <>
      <section id="translationText">
        <ExtraInformation />
        <Dictaphone />
        <TranslatorForm onTranslate={handleTranslatedText} />
        <Footer />
      </section>
    </>
  );
};

export default withAuth(Translator);
