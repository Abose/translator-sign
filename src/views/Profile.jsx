import { NavLink } from "react-router-dom";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import withAuth from "../hoc/withAuth";
import { useUser } from "../state/UserContext";

// profile page

const Profile = () => {
  const { user } = useUser();

  return (
    <>
      <div className="profileTranslate">
        {" "}
        <NavLink to="/translator">
          <img
            className="profileTranslatorlink"
            src="icon/Translate.svg"
            alt="Translatepagrlink"
          />
        </NavLink>
      </div>
      <div className="profileInfo">
        <h2>Profile</h2>
        <img
          className="profilegif"
          src="icon/profile.gif"
          alt="profileimagegif"
        />
        <ProfileHeader username={user.username} />
      </div>
      <ProfileActions />
      <ProfileTranslationHistory translations={user.translations} />
    </>
  );
};

export default withAuth(Profile);
