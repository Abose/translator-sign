import LoginForm from "../components/Login/LogingForm";
// log inn page and everything inside it
const Login = () => {
  return (
    <>
      <div className="logingpage">
        <h1>Sign in</h1>
        <LoginForm />
      </div>
    </>
  );
};

export default Login;
