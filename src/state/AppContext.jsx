import UserProvider from "./UserContext";
// this provide children
const AppContext = ({ children }) => {
  return <UserProvider>{children}</UserProvider>;
};

export default AppContext;
