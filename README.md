# SignLator - Translate from english to english signlangauge (ASL)

this is a project to show what we have learned in React, html, css and link with online api

Made responsive for phones, tablets and desktop

Made for user to sign in with just name and user will be remember if dont sign out, translation is stored in api

## Installation

[Live preview running](https://signlator-production.up.railway.app/)

Install Signlator with gitclone

```bash
  git clone git@gitlab.com:Abose/translator-sign.git
  cd translator
  In terminal type: npm start

  or

  download zip
  cd translator
  In terminal type: npm start
```

## Screenshots

![App Screenshot](https://i.ibb.co/G5HQkYx/localhost-3000-translator-i-Phone-12-Pro.png)
![App Screenshot](https://i.ibb.co/cDjCR9D/localhost-3000-translator-i-Phone-12-Pro-1.png)
![App Screenshot](https://i.ibb.co/CtryLH4/signlator-production-up-railway-app-profile-i-Phone-12-Pro.png)

## 🚀 About Me

i am going on a noroff course to become full stack developer...

## 🛠 Skills

React, Javascript, HTML, CSS...

## Features

- Live previews
- Fullscreen mode
- Cross platform

## Lessons Learned

What did you learn while building this project? What challenges did you face and how did you overcome them?

First challenge where to take a sinle character and turn it onto child element

second challenge where to make use of the child and give it the right image

third inplement Speech to text and make it work

make everything looks pretty depend on which device you use

## 🔗 Links

[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://portfolio.abose.synology.me/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](www.linkedin.com/in/abbas-mehdi-shihab)

## Sources

audio-editing.gif - https://www.flaticon.com/free-animated-icons/sound-waves sound waves animated icons Sound waves animated icons created by Freepik - Flaticon

podcastoff.gif - https://www.flaticon.com/free-animated-icons/microphone microphone animated icons Microphone animated icons created by Freepik - Flaticon

confirm.gif https://s2.ezgif.com/save/ezgif-2-29388db31c.gif Flaticon edited to remove unnessasry and flipped

clear.gif https://lottiefiles.com/web-player?lottie_url=https%3A%2F%2Fassets2.lottiefiles.com%2Fpackages%2Flf20_unfrwh1e.json

space.svg https://iconscout.com/icon/space-bar-3661045

notfound.png https://iconscout.com/icon/error-page-2697301

## License

[MIT](https://choosealicense.com/licenses/mit/)
